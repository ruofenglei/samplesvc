FROM golang:1.12 as build
WORKDIR /code
COPY go.mod go.sum /code/
RUN GOPROXY=https://goproxy.io go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64  go build -o /go/bin/samplesvc

FROM alpine
COPY --from=build /go/bin/samplesvc /go/bin/samplesvc
EXPOSE 8000
ENTRYPOINT ["/go/bin/samplesvc"]
